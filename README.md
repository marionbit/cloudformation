# CloudFormation Demo

[![HIPAA](https://app.soluble.cloud/api/v1/public/badges/503b455c-c9aa-4c6f-b0ff-ad7ee9107118.svg)](https://app.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  [![CIS](https://app.soluble.cloud/api/v1/public/badges/0d39893c-5d2c-41dc-add8-66cc3fad03cc.svg)](https://app.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  [![IaC](https://app.soluble.cloud/api/v1/public/badges/a172514b-dbfc-4c98-8621-454838611293.svg)](https://app.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  

[![CIS](https://app.demo.soluble.cloud/api/v1/public/badges/8c947543-2d35-4aa4-a68e-d67c1a549c00.svg)](https://app.demo.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  [![IaC](https://app.demo.soluble.cloud/api/v1/public/badges/194df1ce-d9b6-48b2-9346-1662527bafed.svg)](https://app.demo.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  [![HIPAA](https://app.demo.soluble.cloud/api/v1/public/badges/c3ded7c9-76b1-4ca8-a743-dc087aebc6c3.svg)](https://app.demo.soluble.cloud/repos/details/github.com/insecurecorp/cloudformation)  

![Sad Cloud](.images/sad-cloud.png)

